import React, { Component } from 'react';

class BookList extends Component {

    render() {
        var listbook = this.props.books.map((books) =>
        <tr key={books.id}>
          <td style={{width: auto , padding: 10 }}>{books.id}</td>
          <td style={{width: auto , padding: 10 }}>{books.name}</td>
          <td style={{width: auto , padding: 10 }}>
              <button style={{background: 'yellow' , padding: 10}}>Hide</button>
          </td>
        </tr>
        )
        return (
            <div>
                <h1>Book List</h1>
                <div>
                <table style={{width: 100%}}>
                    <tr style={{width:100%}}>
                        <th style={{width: auto , padding: 10 }}>#</th>
                        <th style={{width: auto , padding: 10 }}>Title</th>
                        <th style={{width: auto , padding: 10 }}>Published Year</th>
                        <th style={{width: auto , padding: 10 }}>Action</th>
                    </tr>
                    {listbook}
                </table>
        </div>
            </div>
        );
    }
}

export default BookList;